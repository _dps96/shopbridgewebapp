--Insert Initial records to role
INSERT INTO [dbo].[Role](
[Name]			 ,
[Description]	 ,
[DeleteInd]		 ,
[CreatedUserId]  ,
[ModifiedUserId] ,
[CreatedDate] 	 ,
[ModifiedDate]	 
) values('Admin','Admin',0,1,1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

--===========================================================================================================================
--Initial Records to Permissions. Currently we are not autherize user on the basis of permissions, For Future implementations.
--===========================================================================================================================

INSERT INTO [dbo].[Permission](
[Name]			 ,
[Description]	 ,
[DeleteInd]		 ,
[CreatedUserId]  ,
[ModifiedUserId] ,
[CreatedDate] 	 ,
[ModifiedDate]	 
) values('Read','Read',0,1,1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);


INSERT INTO [dbo].[Permission](
[Name]			 ,
[Description]	 ,
[DeleteInd]		 ,
[CreatedUserId]  ,
[ModifiedUserId] ,
[CreatedDate] 	 ,
[ModifiedDate]	 
) values('Update','Update',0,1,1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO [dbo].[Permission](
[Name]			 ,
[Description]	 ,
[DeleteInd]		 ,
[CreatedUserId]  ,
[ModifiedUserId] ,
[CreatedDate] 	 ,
[ModifiedDate]	 
) values('Delete','Delete',0,1,1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);



Declare @RoleId INT=(select RoleID from Role where Name='Admin')
Declare @ReadPermission INT=(select PermissionId from Permission where Name='Read')
Declare @UpdatePermission INT=(select PermissionId from Permission where Name='Update')
Declare @DeletePermission INT=(select PermissionId from Permission where Name='Delete')


--=========================================================================================
-- Assign Permissions for Role, This part we can assign permissions for role from frontend.
--=========================================================================================

INSERT INTO [dbo].[RolePermission](
[RoleId]			 ,
[PermissionId]	 ,
[DeleteInd]		 ,
[CreatedUserId]  ,
[ModifiedUserId] ,
[CreatedDate] 	 ,
[ModifiedDate]	 
) values(@RoleId,@UpdatePermission,0,1,1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO [dbo].[RolePermission](
[RoleId]			 ,
[PermissionId]	 ,
[DeleteInd]		 ,
[CreatedUserId]  ,
[ModifiedUserId] ,
[CreatedDate] 	 ,
[ModifiedDate]	 
) values(@RoleId,@DeletePermission,0,1,1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

INSERT INTO [dbo].[RolePermission](
[RoleId]			 ,
[PermissionId]	 ,
[DeleteInd]		 ,
[CreatedUserId]  ,
[ModifiedUserId] ,
[CreatedDate] 	 ,
[ModifiedDate]	 
) values(@RoleId,@ReadPermission,0,1,1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);

--=============================================================================
--Initial Record to Product Admin as Admin User
--=============================================================================
INSERT INTO [dbo].[Individual](

FirstName	   ,
LastName	   ,
DateOFBirth	   ,
EmailId		   ,
PhoneNumber	   ,
DeleteInd	   ,
CreatedUserId  ,
ModifiedUserId ,
CreatedDate	   ,
ModifiedDate)
values('Product','Admin','2000-11-01','sysadmin@thinkbridge.in','8299147121',0,1,1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);



Declare @IndividualId INT=(select IndividualId from Individual where FirstName='Product' and LastName='Admin')




Insert into dbo.[User](
[RoleId]		,
[IndividualId]	  ,
[Password]		  ,
[DeleteInd]		  ,
[CreatedUserId]   ,
[ModifiedUserId]  ,
[CreatedDate] 	  ,
[ModifiedDate]
)
Values(@RoleId,@IndividualId,'Sys123!',0,1,1,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);
