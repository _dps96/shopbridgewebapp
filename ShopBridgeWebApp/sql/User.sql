USE [ShopBridgeDb]
GO

/****** Object:  Table [dbo].[User]    Script Date: 10/24/2021 10:07:27 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[IndividualId] [int] NOT NULL,
	[DeleteInd] [tinyint] NOT NULL,
	[CreatedUserId] [int] NOT NULL,
	[ModifiedUserId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Password] [varchar](50) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_Individual_IndividualId] FOREIGN KEY([IndividualId])
REFERENCES [dbo].[Individual] ([IndividualId])
GO

ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_Individual_IndividualId]
GO

ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_Role_RoleId_] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([RoleId])
GO

ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_Role_RoleId_]
GO


