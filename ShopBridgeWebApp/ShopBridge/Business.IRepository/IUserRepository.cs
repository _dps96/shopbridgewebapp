﻿using Business.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.IRepository
{
    public interface IUserRepository
    {
        User getuserInfo(string Username, string Password);
        Role getUserRole(string Username);
    }
}
