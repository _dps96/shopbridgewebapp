﻿using Business.Entity;
using System.Collections.Generic;

namespace Business.IRepository
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> GetCategories();
    }
}
