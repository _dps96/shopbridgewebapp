﻿using Business.Entity;
using System.Collections.Generic;

namespace Business.IRepository
{
    public interface IProductRepository
    {
        IEnumerable<Product> GetProducts();
        IEnumerable<Product> GetProductByCategoryId(int categoryId);
        Product AddProduct(Product newProduct);
        Product UpdateProduct(Product existingProduct);
        string RemoveProduct(int productId);
        Product GetProductByProductid(int ProductID);
    }
}
