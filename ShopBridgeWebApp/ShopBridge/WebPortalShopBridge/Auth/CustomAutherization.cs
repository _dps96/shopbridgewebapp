﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WebPortalShopBridge.Wrapper;

namespace WebPortalShopBridge.Auth
{
    public class CustomAutherization : AuthorizeAttribute

    {
        private readonly string[] allowedroles;
        public CustomAutherization(params string[] roles)
        {
            this.allowedroles = roles;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;
            var userId = Convert.ToString(httpContext.Session["UserId"]);
            if (!string.IsNullOrEmpty(userId))
            {
                var userRole = UserWrapper.GetUserRole(userId);
                foreach (var role in allowedroles)
                {
                    if (role == userRole.Name) return true;
                }
            }
            return authorize;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
               new RouteValueDictionary
               {
                    { "controller", "Home" },
                    { "action", "UnAuthorized" }
               });
        }
    }
}