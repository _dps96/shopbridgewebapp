﻿using Business.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPortalShopBridge.Auth;
using WebPortalShopBridge.Wrapper;

namespace WebPortalShopBridge.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {
        public ActionResult Login()
        {
            if(!string.IsNullOrWhiteSpace(Convert.ToString(Session["UserId"])))
                return RedirectToAction("Index", "Product");
            return View();
        }
        [HttpPost]
        public ActionResult Login(User model)
        {
            if (ModelState.IsValid)
            {
                User user = UserWrapper.GetUserInfo(model.Email, model.Password);
                if (user != null)
                {
                    Session["Username"] = user.UserName;
                    Session["UserId"] = user.Email;
                    return RedirectToAction("Index", "Product");
                }
                else
                    ModelState.AddModelError("", "Invalid User Name or Password");
            }
            
                model.Email = "";
                model.Password = "";
                Session["Username"] = "";
                Session["UserId"] = "";
                return View(model);
                
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            Session["UserName"] = string.Empty;
            Session["UserId"] = string.Empty;
            return RedirectToAction("Login", "Home");
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        [CustomAuthentication]
        [CustomAutherization("Customer")]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult UnAuthorized()
        {
            ViewBag.Message = "Un Authorized Page!";

            return View();
        }
    }
}