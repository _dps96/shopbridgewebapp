﻿using Business.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebPortalShopBridge.Auth;
using WebPortalShopBridge.Helper;
using WebPortalShopBridge.Models;
using WebPortalShopBridge.Wrapper;

namespace WebPortalShopBridge.Controllers
{
    [CustomAuthentication]
    public class ProductController : Controller
    {
        // GET: Product
        [CustomAutherization("Admin")]
        public ActionResult Index()
        {
            var productModel = new ProductCategoryViewModel()
            {
                Categories = CategoryWrapper.GetCategories(),
                Products = ProductWrapper.GetProducts()
            };
            return View(productModel);
        }
        public ActionResult DisplayProductByCategory(int categoryId)
        {
            var productModel = new ProductCategoryViewModel()
            {
                Categories = CategoryWrapper.GetCategories(),
                Products = ProductWrapper.GetProductByCategoryId(categoryId)
            };

            return PartialView("~/Views/Product/Index.cshtml", productModel);
        }

        [CustomAutherization("Admin")]
        public  ActionResult AddProduct()
        {
            var categoryList = CategoryWrapper.GetCategories().Select(x => new KeyValuePair<int, string>(x.CategoryId, x.Name)).ToList();
            categoryList.Insert(0, new KeyValuePair<int, string>(0,"Select"));
            var model = new ProductViewModel()
            {
                Categories = categoryList
            };

            return View(model);

        }
        [CustomAutherization("Admin")]
        [HttpPost]
        [HandleError(View = "~/Views/Shared/Error.cshtml")] 
        public  ActionResult AddProduct(FormCollection formCollection, HttpPostedFileBase file)
        {
            bool isError = false;
            var categoryList = CategoryWrapper.GetCategories().Select(x => new KeyValuePair<int, string>(x.CategoryId, x.Name)).ToList();
            categoryList.Insert(0, new KeyValuePair<int, string>(0, "Select"));
            var model = new ProductViewModel()
            {
                Categories = categoryList
            };
            var allowedExtensions = new[] {
            ".jpg", ".png", "jpeg"
                    };
            
             if (file == null)
            {
                ModelState.AddModelError("", "Choose File");
                isError = true;
            }
            else if (file != null && !allowedExtensions.Contains(Path.GetExtension(file.FileName).ToString().ToLower()))
            {
                ModelState.AddModelError("", "Acceptable Image format *.Jpg,*.Jpeg,*.png.");
                isError = true;
            }
            else if (Convert.ToInt32(formCollection["SelectedCategoryId"].ToString()) == 0)
            {
                ModelState.AddModelError("", "Select category.");
                isError = true;
            }else if (string.IsNullOrWhiteSpace(formCollection["Name"].ToString()))
            {
                ModelState.AddModelError("", "Name Cannot be empty.");
                isError = true;
            }
            else if (string.IsNullOrWhiteSpace(formCollection["Description"].ToString()))
            {
                ModelState.AddModelError("", "Description Cannot be empty.");
                isError = true;
            }

            if (!isError)
            {
                ProductWrapper.AddProduct(ProcessImage.AddProduct(formCollection, file));
                return RedirectToAction("Index", "Product");
            }
            return View(model);

        }
        public ActionResult UpdateProduct(int ProductId) {
            var model = new ProductViewModel
            {
                Categories= CategoryWrapper.GetCategories().Select(x => new KeyValuePair<int, string>(x.CategoryId, x.Name)).ToList(),
                Product = ProductWrapper.GetProductByProductid(ProductId),
            };
            model.SelectedCategoryId = model.Product.CategoryId;
                return View(model);
        }
        [HttpPost]
        public ActionResult UpdateProduct(FormCollection formCollection) {
            bool isError = false;
            if (Convert.ToInt32(formCollection["SelectedCategoryId"].ToString()) == 0)
            {
                ModelState.AddModelError("", "Select category.");
                isError = true;
            }
            else if (string.IsNullOrWhiteSpace(formCollection["Name"].ToString()))
            {
                ModelState.AddModelError("", "Name Cannot be empty.");
                isError = true;
            }
            else if (string.IsNullOrWhiteSpace(formCollection["Description"].ToString()))
            {
                ModelState.AddModelError("", "Description Cannot be empty.");
                isError = true;
            }

            var existingProduct = new Product
            {
                ProductId=Convert.ToInt32(formCollection["ProductId"].ToString()),
                Name= formCollection["Name"].ToString(),
                Description= formCollection["Description"].ToString(),
                Price= Convert.ToDecimal(formCollection["Price"].ToString()),
                CategoryId= Convert.ToInt32(formCollection["SelectedCategoryId"].ToString()),
                AvailableStock = Convert.ToInt32(formCollection["Availability"].ToString())

            };
            existingProduct=ProductWrapper.UpdateProduct(existingProduct);
            if (existingProduct == null){

                ModelState.AddModelError("", "This Product is not present. Create new one");
                isError = true;
            }
            if (!isError)
            {
                return RedirectToAction("Index", "Product");
            }
            var model = new ProductViewModel
            {
                Categories = CategoryWrapper.GetCategories().Select(x => new KeyValuePair<int, string>(x.CategoryId, x.Name)).ToList()
            };
            model.SelectedCategoryId = model.Product.CategoryId;
            return View(model);
        }
        [HttpPost]
        public ActionResult RemoveProduct(int ProductId)
        {
            ProductWrapper.RemoveProduct(ProductId);
            return RedirectToAction("Index", "Product");
        }
    }
}