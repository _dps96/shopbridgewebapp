﻿using Business.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebPortalShopBridge.Helper
{
    public static class ProcessImage
    {
        public static Product AddProduct(FormCollection formCollection, HttpPostedFileBase file)
        {
            Product product = new Product();
            var allowedExtensions = new[] {
            ".jpg", ".png", "jpeg"
                    };
            var Id = formCollection["ProductId"].ToString();
            
            product.CategoryId = Convert.ToInt32(formCollection["SelectedCategoryId"].ToString());
            var Image_url = file.ToString(); //getting complete url  
            product.Name = formCollection["Name"].ToString();
            product.Description = formCollection["Description"].ToString();
            product.Price = Convert.ToInt32(formCollection["Price"].ToString());
            product.AvailableStock = Convert.ToInt32(formCollection["Price"].ToString());
            var fileName = Path.GetFileName(file.FileName); //getting only file name(ex-ganesh.jpg)  
            var ext = Path.GetExtension(file.FileName); //getting the extension(ex-.jpg)  
            if (allowedExtensions.Contains(ext.ToString().ToLower())) //check what type of extension  
            {
                string name = Path.GetFileNameWithoutExtension(fileName); //getting file name without extension  
                string myfile = name + "_" + Id + ext; //appending the name with id  
                                                       // store the file inside ~/project folder(Img)
                var path = Path.Combine(HttpContext.Current.Server.MapPath("~/Img"), myfile);
                file.SaveAs(path);
                product.Url = myfile;
            }
            return product;
        }
    }
}