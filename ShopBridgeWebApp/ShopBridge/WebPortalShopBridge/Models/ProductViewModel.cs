﻿using Business.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPortalShopBridge.Models
{
    public class ProductViewModel
    {
        public Product Product  { get; set; }
        public IEnumerable<KeyValuePair<int,string>> Categories { get; set; }
        public int SelectedCategoryId { get; set; }
    }
}