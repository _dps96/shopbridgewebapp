﻿using Business.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPortalShopBridge.Models
{
    public class ProductCategoryViewModel
    {
        public IEnumerable<Category> Categories { get; set; }
        public IEnumerable<Product> Products { get; set; }
    }
}