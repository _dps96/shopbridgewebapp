﻿using Business.Entity;
using Business.IRepository;
using Business.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPortalShopBridge.Wrapper
{
    public static class UserWrapper
    {
        public static User GetUserInfo(string username, string password)
        {
            IUserRepository userRepository = new UserRepository();
            return userRepository.getuserInfo(username, password);

        }
        public static Role GetUserRole(string username)
        {
            IUserRepository userRepository = new UserRepository();
            return userRepository.getUserRole(username);

        }
    }
}