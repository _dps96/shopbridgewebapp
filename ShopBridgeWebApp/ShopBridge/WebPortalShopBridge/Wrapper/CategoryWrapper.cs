﻿using Business.Entity;
using Business.IRepository;
using Business.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPortalShopBridge.Wrapper
{
    public static class CategoryWrapper
    {
        public static IEnumerable<Category> GetCategories()
        {
            ICategoryRepository categoryRepository = new CategoryRepository();
            return categoryRepository.GetCategories();
        }
    }
}