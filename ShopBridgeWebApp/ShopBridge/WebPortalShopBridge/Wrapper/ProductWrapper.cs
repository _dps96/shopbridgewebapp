﻿using Business.Entity;
using Business.IRepository;
using Business.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPortalShopBridge.Wrapper
{
    public class ProductWrapper
    {
        
        public static IEnumerable<Product> GetProducts() {
            IProductRepository productRepository = new ProductRepository();
            return productRepository.GetProducts();
        }
        public static IEnumerable<Product> GetProductByCategoryId(int categoryId) {
            IProductRepository productRepository = new ProductRepository();
            return productRepository.GetProductByCategoryId(categoryId);
        }
        public static Product AddProduct(Product newProduct) {
            IProductRepository productRepository = new ProductRepository();
            return productRepository.AddProduct(newProduct);
        }
        public static Product GetProductByProductid(int ProductID)
        {
            IProductRepository productRepository = new ProductRepository();
            return productRepository.GetProductByProductid(ProductID);

        }
        public static Product UpdateProduct(Product existingProduct) {
            IProductRepository productRepository = new ProductRepository();
            return productRepository.UpdateProduct(existingProduct);
        }
        public static string RemoveProduct(int productId)
        {
            IProductRepository productRepository = new ProductRepository();
            return productRepository.RemoveProduct(productId);
        }


    }
}