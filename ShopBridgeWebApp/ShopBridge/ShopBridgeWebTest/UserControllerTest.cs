﻿using Business.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Web.Mvc;
using WebPortalShopBridge.Controllers;

namespace ShopBridgeWebTest
{
    [TestClass]
    public class UserControllerTest
    {
        [TestMethod]
        public void TestLoginView()
        {
            var controller = new HomeController();
            var model = new User { Email = "sysadmin@thinkbridge.in", Password = "Sys123!" };
            var result = controller.Login(model) as ViewResult;
            Assert.AreEqual("Login", result.ViewName);
        }
        [TestMethod]
        public void TestLoginRedirect()
        {
            var controller = new HomeController();
            var model = new User { Email = "sysadmin@thinkbridge.in", Password = "Sys123!" };
            var result = (RedirectToRouteResult)controller.Login(model);
            Assert.AreEqual("Index", result.RouteValues["action"]);

        }
        [TestMethod]
        public void TestDetailsViewData()
        {
            var controller = new HomeController();
            var model = new User { Email = "sysadmin@thinkbridge.in", Password = "Sys123!" };
            var result = controller.Login(model) as ViewResult;
            var user = (User)result.ViewData.Model;
            Assert.AreEqual("Product Admin", user.UserName);
        }

    }
}
