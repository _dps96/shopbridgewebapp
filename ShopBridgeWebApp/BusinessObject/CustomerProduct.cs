﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entity
{
    public class CustomerProduct
    {
       
        public int CustomerProductId { get; set; }
        public int UserId { get; set; }
        public int ProductId { get; set; }

        public bool DeleteInd { get; set; }
        public int CreatedUserId { get; set; }
        public int ModifiedUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
