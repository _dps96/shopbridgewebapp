﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entity
{
    public class Category
    { 
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public byte DeleteInd { get; set; }
        public int CreatedUserId { get; set; }
        public int ModifiedUserId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public virtual ICollection<Product> Products { get; set; }

    }
}
