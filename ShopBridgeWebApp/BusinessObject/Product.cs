﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entity
{
    public class Product
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int AvailableStock { get; set; }
        public bool DeleteInd { get; set; }
        public int CreatedUserId { get; set; }
        public int ModifiedUserId  { get; set; }
        public DateTime CreatedDate     { get; set; }
        public DateTime ModifiedDate { get; set; }
        public virtual Category category { get; set; }

    }
}
