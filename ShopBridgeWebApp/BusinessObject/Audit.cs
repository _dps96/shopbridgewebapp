﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entity
{
    public class Audit
    {
        public int AuditId { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }
        public DateTime AuditDate { get; set; }
        public bool DeleteInd { get; set; }
        
    }
}
