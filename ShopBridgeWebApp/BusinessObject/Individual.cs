﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entity
{
    public class Individual
    {
        public int IndividualId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime DateOFBirth { get; set; }
        public string EmailId { get; set; }
        public string PhoneNumber { get; set; }
        public bool DeleteInd { get; set; }
        public int CreatedUserId { get; set; }
        public int ModifiedUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
