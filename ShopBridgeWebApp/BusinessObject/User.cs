﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Entity
{
    public class User
    {
        public int UserId { get; set; }
        public int IndividualId { get; set; }
        public int RoleId { get; set; }
        [Required(ErrorMessage = "{0} must not be empty.")]
        [Display(Name = "Password")]

        public string Password { get; set; }
        public bool DeleteInd { get; set; }
        public int CreatedUserId { get; set; }
        public int ModifiedUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Required(ErrorMessage = "{0} must not be empty.")]
        [RegularExpression(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", ErrorMessage = "Invalid Username.")]
        [Display(Name ="Username")]
        public virtual string Email{get;set;}
        public virtual string UserName { get; set; }



    }
}
