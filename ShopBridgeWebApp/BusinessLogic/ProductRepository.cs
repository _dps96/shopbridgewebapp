﻿using Business.Entity;
using Business.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository
{
    public class ProductRepository : IProductRepository
    {
        private Data.Entity.ShopBridgeDbEntities shopBridgeDbEntities;
        public ProductRepository()
        {
            shopBridgeDbEntities = new Data.Entity.ShopBridgeDbEntities();

        }
        public Product AddProduct(Product newProduct)
        {
            if(newProduct == null)
            {
                return newProduct;
            }
            var product = new Data.Entity.Product()
            {
                Name=newProduct.Name,
                Description=newProduct.Description,
                CategoryId=newProduct.CategoryId,
                Price=newProduct.Price,
                AvailableStock=newProduct.AvailableStock,
                CreatedDate=DateTime.Now,
                ModifiedDate=DateTime.Now,
                CreatedUserId=1,
                ModifiedUserId=1,
                Url=newProduct.Url
            };
            product=shopBridgeDbEntities.Products.Add(product);
            shopBridgeDbEntities.SaveChanges();
            newProduct.ProductId = product.ProductId;
            return newProduct;
        }

        public IEnumerable<Product> GetProductByCategoryId(int categoryId)
        {
            return shopBridgeDbEntities.Products.Where(x => x.CategoryId == categoryId).Select(x => new Product
            {
                ProductId=x.ProductId,
                Name = x.Name,
                Description = x.Description,
                Price = x.Price,
                AvailableStock = x.AvailableStock,
                Url = x.Url,
                category = new Category { Name = x.Category.Name, Description = x.Category.Description, CategoryId = x.Category.CategoryId }
            }).ToList();
        }

        public IEnumerable<Product> GetProducts()
        {
            return shopBridgeDbEntities.Products.Include("Category").Select(x => new Product
            {
                ProductId = x.ProductId,
                Name = x.Name,
                Description = x.Description,
                Price = x.Price,
                AvailableStock = x.AvailableStock,
                Url=x.Url,
                category=new Category { Name=x.Category.Name,Description=x.Category.Description,CategoryId=x.Category.CategoryId}
            }).ToList();
        }
         public Product GetProductByProductid(int ProductID)
        {
            return shopBridgeDbEntities.Products.Include("Category").Where(x=>x.ProductId==ProductID).Select(x => new Product
            {
                ProductId = x.ProductId,
                Name = x.Name,
                Description = x.Description,
                Price = x.Price,
                AvailableStock = x.AvailableStock,
                CategoryId=x.CategoryId,
                Url=x.Url,
                category=new Category { Name=x.Category.Name,Description=x.Category.Description,CategoryId=x.Category.CategoryId}
            }).FirstOrDefault();
        }

        public string RemoveProduct(int productId)
        {
             if(productId==0)
            {
                return "0";
            }
            var objProduct=shopBridgeDbEntities.Products.Where(x => x.ProductId == productId && x.DeleteInd==0).FirstOrDefault();
            if(objProduct == null)
            {
                return null;//Product is not found
            }
            shopBridgeDbEntities.Products.Remove(objProduct);
            shopBridgeDbEntities.SaveChanges();
            return "1";
        }

        public Product UpdateProduct(Product existingProduct)
        {
            if (existingProduct == null || existingProduct.ProductId == 0)
            {
                return null;
            }
            var objProduct = shopBridgeDbEntities.Products.Where(x => x.ProductId == existingProduct.ProductId && x.DeleteInd == 0).FirstOrDefault();
            if (objProduct == null)
            {
                return null;//Product is not found
            }
            objProduct.Name = existingProduct.Name;
            objProduct.Description = existingProduct.Description;
            objProduct.DeleteInd = Convert.ToByte(existingProduct.DeleteInd);
            objProduct.AvailableStock = existingProduct.AvailableStock;
            objProduct.Price = existingProduct.Price;
            objProduct.CategoryId = existingProduct.CategoryId;
            objProduct.ModifiedDate = DateTime.Now;
            shopBridgeDbEntities.SaveChanges();
            return existingProduct;

        }
    }
}
