﻿using Business.IRepository;
using Data.Entity;

using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Repository
{
    public class UserRepository : IUserRepository
    {
        private ShopBridgeDbEntities shopBridgeDbEntities;
        public UserRepository()
        {
            shopBridgeDbEntities = new ShopBridgeDbEntities();
        }

        public Entity.User getuserInfo(string Username, string Password)
        {
            try
            {
                var user= shopBridgeDbEntities.Users.Where(x => x.Individual.EmailId.Equals(Username)
                                                            && x.Password.Equals(Password)).
                                                            Select(x => new Entity.User
                                                            {
                                                                Email = x.Individual.EmailId,
                                                                CreatedDate = x.CreatedDate,
                                                                ModifiedDate = x.ModifiedDate,
                                                                UserName=x.Individual.FirstName + " " + x.Individual.LastName
                                                            }
                                                            ).FirstOrDefault();
                return user;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }
        public Entity.Role getUserRole(string Username)
        {
            try
            {
                var user= shopBridgeDbEntities.Users.Join(shopBridgeDbEntities.Roles,
                    userRoleId=>userRoleId.RoleId,
                    roleID=>roleID.RoleId,
                    (userRoleId,roleId)=>new 
                    {
                        Name=roleId.Name,
                        Email=userRoleId.Individual.EmailId,
                        Description=roleId.Description
                    }). Where(x => x.Email.Equals(Username)
                                                            ).
                                                            Select(x => new Entity.Role
                                                            {
                                                                Name = x.Name,
                                                                Description = x.Description
                                                            }
                                                            ).FirstOrDefault();
                return user;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
