﻿using Business.IRepository;
using Data.Entity;
using System.Collections.Generic;
using System.Linq;

namespace Business.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        private ShopBridgeDbEntities shopBridgeDbEntities;
        public CategoryRepository()
        {
            shopBridgeDbEntities = new ShopBridgeDbEntities();
        }
        
        public IEnumerable<Business.Entity.Category> GetCategories()
        {
            return shopBridgeDbEntities.Categories.Select(x=>new Business.Entity.Category { 
                                                                    CategoryId=x.CategoryId,
                                                                    Name=x.Name,
                                                                    Description=x.Description,
                                                                    DeleteInd=x.DeleteInd
                                                                    }).ToList();
        }
    }
}
